################################### THIS ENEMY IS FOR LEVELS 1-5 ###################################
import random

def low_enemy_spawn():
    enemy1 = low_enemy_stats
    enemy2 = low_enemy_stats
    enemy3 = low_enemy_stats


low_enemy_stats = {
    "enemylevel": random.randint(1,5),
    "enemyhealth": random.randint(5,10),
    "enemystrength": 3,
    "enemygold": random.randint(50,100),
}
