################################### THIS ENEMY IS FOR LEVELS 11-15 ###################################
import random

def high_enemy_spawn():
    enemy1 = high_enemy_stats
    enemy2 = high_enemy_stats
    enemy3 = high_enemy_stats


high_enemy_stats = {
    "enemylevel": random.randint(1,5),
    "enemyhealth": random.randint(5,10),
    "enemystrength": 10,
    "enemygold": random.randint(50,100),
}
