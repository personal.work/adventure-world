################################### THIS ENEMY IS FOR LEVELS 1-5 ###################################
import random

def insane_enemy_spawn():
    enemy2 = insane_enemy_stats
    enemy3 = insane_enemy_stats
    enemy1 = insane_enemy_stats


insane_enemy_stats = {
    "enemylevel": random.randint(1,5),
    "enemyhealth": random.randint(5,10),
    "enemystrength": 3,
    "enemygold": random.randint(50,100),
}
