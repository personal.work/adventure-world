################################### THIS ENEMY IS FOR LEVELS 6-10 ###################################
import random

def mid_enemy_spawn():
    enemy1 = mid_enemy_stats
    enemy2 = mid_enemy_stats
    enemy3 = mid_enemy_stats


mid_enemy_stats = {
    "enemylevel": random.randint(1,5),
    "enemyhealth": random.randint(5,10),
    "enemystrength": 7,
    "enemygold": random.randint(50,100),
}
